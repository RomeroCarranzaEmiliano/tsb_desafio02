import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    static int []lengths = new int[]{30000, 300000, 15000, 150000, 1500000};
    static String []filenames = new String[]{"lote01.txt", "lote02.txt", "mss01.txt", "mss02.txt", "mss03.txt"};

    public static void main(String []args) {

        menu();
    }

    public static void menu() {
        Scanner scan = new Scanner(System.in);

        ArrayList<Integer> numbersLote01 = null;
        ArrayList<Integer> numbersLote02 = null;
        ArrayList<Integer> numbersMss01 = null;
        ArrayList<Integer> numbersMss02 = null;
        ArrayList<Integer> numbersMss03 = null;

        int option = -1;
        int result;

        while (option != 0) {
            System.out.println("Seleccione lote a calcular");
            System.out.println("(1) lote01");
            System.out.println("(2) lote02");
            System.out.println("(3) mss02");
            System.out.println("(0) Salir");
            System.out.println(">> ");

            option = scan.nextInt();

            switch (option) {
                case 1:
                    if (numbersLote01 == null) {
                        numbersLote01 = scanNumbers(1);
                    }
                    //result = calculateInversions(numbersLote01);
                    result = (int)contar(numbersLote01);
                    System.out.println("Inversiones en lote01: "+result);
                    break;
                case 2:
                    if (numbersLote02 == null) {
                        numbersLote02 = scanNumbers(2);
                        result = calculateInversions(numbersLote02);
                        //result = (int)contar(numbersLote02);
                        System.out.println("Inversiones en lote02: "+result);
                    }
                    break;
                case 3:
                    if (numbersMss01 == null) {
                        numbersMss01 = scanNumbers(3);
                        result = (int)maxSumSubsequence3(numbersMss01);
                        System.out.println("Inversiones en mss01: "+result);
                    }
                    break;
                case 4:
                    if (numbersMss02 == null) {
                        numbersMss02 = scanNumbers(4);
                        result = (int)maxSumSubsequence3(numbersMss02);
                        System.out.println("Inversiones en mss02: "+result);
                    }
                    break;
                case 5:
                    if (numbersMss03 == null) {
                        numbersMss03 = scanNumbers(5);
                        result = (int)maxSumSubsequence3(numbersMss03);
                        System.out.println("Inversiones en mss03: "+result);
                    }
                    break;
            }

        }
    }

    public static int calculateInversions(ArrayList<Integer> numbers) {
        int count = 0;

        for (int i=0; i < numbers.size()-1; i++) {
            for (int j=i+1; j < numbers.size(); j++) {
                if (numbers.get(i) > numbers.get(j)) {
                    count += 1;
                }
            }
            System.out.println(i);
        }

        return count;

    }

    public static long maxSumSubsequence(ArrayList<Integer> numbers) {
        long mss = 0;

        for (int i=0; i<numbers.size()-1; i++) {
            for (int j=i; j<numbers.size()-1; j++) {
                int sp = 0;
                for (int k=i; k<j; k++) {
                    sp = sp + numbers.get(k);
                }

                if (sp > mss) {
                    mss = sp;
                }
            }
            System.out.println(i);
        }

        return mss;
    }

    public static long maxSumSubsequence2(ArrayList<Integer> numbers) {
        long mss = 0;

        for (int i=0; i<numbers.size()-1; i++) {
            int sp = 0;
            for (int j=i; j<numbers.size()-1; j++) {
                sp += numbers.get(j);

                if (sp > mss) {
                    mss = sp;
                }
            }
            System.out.println(i);
        }

        return mss;
    }

    public static long maxSumSubsequence3(ArrayList<Integer> numbers) {
        long mss = 0;
        int sp = 0;

        for (int j=0; j<numbers.size()-1; j++) {
            sp = sp + numbers.get(j);

            if (sp > mss) {
                mss = sp;
            } else if (sp < 0) {
                sp = 0;
            }

            System.out.println(j);
        }

        return mss;
    }

    public static long contar(ArrayList<Integer> v)
    {
        int n = v.size();
        long c = 0;
        for( int i = 0; i < n - 1; i++ )
        {
            for( int j = i + 1; j < n; j++ )
            {
                if( v.get(i) > v.get(j) )
                {
                    c++;
                    int aux = v.get(i);
                    v.add(i, v.get(j));
                    v.add(j, aux);
                }
            }
            System.out.println(i);
        }
        return c;
    }



    public static ArrayList<Integer> scanNumbers(int fileNumber) {
        fileNumber -= 1;

        final ArrayList<Integer> numbers = new ArrayList<Integer>(lengths[fileNumber]);

        System.out.println(filenames[fileNumber]);

        try {
            Scanner scan = new Scanner(new File(filenames[fileNumber]));

            for (int i = 0; i<lengths[fileNumber]; i++) {
                numbers.add(Integer.parseInt(scan.nextLine()));
            }

        } catch (FileNotFoundException e) {
            System.err.println("[!] File not found.");
        }

        return numbers;

    }

}
